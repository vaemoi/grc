# GRC - Git Remote Creation (THIS TOOL IS UNSTABLE)

This was the original idea behind grc, written in zsh, and while I do try and update it from time to time with new features from grc-js  I do so just to keep my shell scripting skills sharp. Refer to [grc-js](https://bitbucket.org/moiinc/grc-js) for a more stable experience!

A simple command line tool to create/modify/delete your remotes.

## Getting Started

Here's what you need to get started:

- A GitHub and/or bitbucket account
- personal access token (for GitHub auth)
- git

### Instructions

#### Github

1. Go to [github](https://github.com) and login/create account.
2. Create a [token](https://github.com/settings/tokens).
    2a. Give scope *repo* for public **and** private repo access.
    2b. Give scope *pubilc_repo* for public repo access.
3. Edit the script by replacing USER and TOKEN or use flags -u and -t to specify at run time.

#### Bitbucket

1. Go to [bitbucket](https://bitbucket.org) and login/create account.
2. Edit the script by replacing USER and TOKEN or use flags -u and -t to specify at run time.
3. **Note Bitbucket's TOKEN is actually just your password.

###Flags
* -h g,b - Select which you want to modify (bitbucket or github)
* -n - Name of repo
* -p - Set repo to private
* -u - Username for respective host
* -t - Token for respective host
* --g - Set host to github
* --b - Set host to bitbucket